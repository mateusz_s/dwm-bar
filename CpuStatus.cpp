#include "CpuStatus.hpp"
#include "Icons.hpp"
#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>
#include <numeric>
#include <regex>
#include <unistd.h>

#include "Colors.hpp"

CpuStatus::CpuStatus() {
    nrOfCpus = 1; // determineCpusNumber();
    previousTotal.resize(nrOfCpus);
    previousUsage.resize(nrOfCpus);
}

const std::string &CpuStatus::getString() {
    return cpuString;
}

int CpuStatus::determineCpusNumber() {
    std::ifstream file("/proc/stat");
    if (!file.is_open()) {
        cpuString = " cpu: - ";
    }
    std::string read;
    std::regex cpuRegex("cpu.*");
    int cpus = 0;
    do {
        file >> read;
        if (!std::regex_match(read, cpuRegex))
            break;
        float tmp = 0;
        for (int i = 0; i < 10; i++) {
            file >> tmp;
        }
        cpus++;
    } while (true);
    file.close();
    return cpus;
}

void CpuStatus::update() {
    cpuString = CPU_ICON;
    cpuString += GREEN_ON_BLACK;
    std::ifstream file("/proc/stat");
    if (!file.is_open()) {
        cpuString = " cpu: - ";
    }
    std::string read;
    for (int cpuId = 0; cpuId < nrOfCpus; cpuId++) {
        file >> read;

        unsigned int tmp;
        std::vector<unsigned int> times;
        for (int i = 0; i < 10; i++) {
            file >> tmp;
            times.push_back(tmp);
        }

        unsigned int total = std::accumulate(times.begin(), times.end(), 0);
        unsigned int idle = times[3];

        float diff_total = total - previousTotal[cpuId];
        float diff_idle = idle - previousUsage[cpuId];

        previousTotal[cpuId] = total;
        previousUsage[cpuId] = idle;

        int usage =
            (float)sysconf(_SC_CLK_TCK) * (diff_total - diff_idle) / diff_total;

        if (usage < 10)
            cpuString += "  ";
        else if (usage < 100)
            cpuString += " ";

        if (usage > 50 && usage < 90) {
            cpuString += YELLOW_ON_BLACK;
        } else if (usage > 90) {
            cpuString += RED_ON_BLACK;
        }
        cpuString += std::to_string(usage);
    }

    cpuString += "%";
    cpuString += DEFAULT;
    file.close();
}
