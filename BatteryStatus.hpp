#pragma once 

#include <string>

#include "SystemStatus.hpp"

#define BATTERY_STATUS_NOW_PATH "/sys/class/power_supply/BAT1/charge_now"
#define BATTERY_STATUS_MAX_PATH "/sys/class/power_supply/BAT1/charge_full"

class BatteryStatus : public SystemStatus {
    public:
        const std::string &getString();
        void update();
        ~BatteryStatus() {};
    private:
        std::string batteryString;
};
