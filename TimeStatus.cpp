#include "TimeStatus.hpp"
#include "Icons.hpp"
#include <ctime>
#include <iomanip>
#include <string>

#define TIME_BUFFER_SIZE 18

void TimeStatus::update() {
    auto t = std::time(nullptr);
    char buffer[TIME_BUFFER_SIZE];
    struct tm *currentTime = std::localtime(&t);
    std::strftime(buffer, TIME_BUFFER_SIZE, "%m/%d/%y %H:%M:%S", currentTime);
    timeString = CLOCK_ICON;
    timeString += buffer;
}

const std::string &TimeStatus::getString() {
    return timeString;
}
