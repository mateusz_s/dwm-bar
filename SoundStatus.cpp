#include "SoundStatus.hpp"
#include "Icons.hpp"	
#include <alsa/asoundlib.h>
#include <alsa/control.h>

void SoundStatus::update() {
    long min, max, volume = 0;
    snd_mixer_t *handle;
    snd_mixer_selem_id_t *sid;
    const char *card = "default";
    const char *selem_name = "PCM";

    snd_mixer_open(&handle, 0);
    snd_mixer_attach(handle, card);
    snd_mixer_selem_register(handle, nullptr, nullptr);
    snd_mixer_load(handle);

	snd_mixer_selem_id_alloca(&sid);
    snd_mixer_selem_id_set_index(sid, 0);
    snd_mixer_selem_id_set_name(sid, selem_name);
    snd_mixer_elem_t* elem = snd_mixer_find_selem(handle, sid);
    if(elem != nullptr) {
        snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
	    snd_mixer_selem_get_playback_volume(elem, SND_MIXER_SCHN_MONO, &volume);
        snd_mixer_close(handle);

        auto vol = (static_cast<double>(volume) / max) * 100;
	    if(vol > 0) {
		    soundString = SOUND_ICON;
	    }
	    else {
		    soundString = SOUND_MUTE_ICON;
	    }
	    soundString += std::to_string(static_cast<int>(vol));
    }
    else {
        soundString = SOUND_MUTE_ICON; 
    }

}

const std::string &SoundStatus::getString() {
	return soundString;
}
