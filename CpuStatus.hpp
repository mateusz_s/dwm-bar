#pragma once
#include <vector>

#include "SystemStatus.hpp"

class CpuStatus: public SystemStatus {
public:
    CpuStatus();
    const std::string &getString();
    void update();
    ~CpuStatus() {};
private:
    std::string cpuString;
    int nrOfCpus;
    std::vector<unsigned int> previousTotal;
    std::vector<unsigned int> previousUsage;
    int determineCpusNumber();
};
