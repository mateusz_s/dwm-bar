#pragma once

#include "SystemStatus.hpp"

class TimeStatus : public SystemStatus {
public:
    const std::string &getString();
    void update();
    ~TimeStatus() {};
private:
    std::string timeString;
};
