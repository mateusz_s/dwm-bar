#include <fstream>

#include "BatteryStatus.hpp"
#include "Colors.hpp"
#include "Icons.hpp"

const std::string &BatteryStatus::getString() {
    return batteryString;
}

void BatteryStatus::update() {
    batteryString = "";
    std::ifstream maxChargeFile(BATTERY_STATUS_MAX_PATH);
    if (!maxChargeFile.is_open()) {
        batteryString = "nobat";
        return;
    }
    long chargeFull = 0;
    maxChargeFile >> chargeFull;
    maxChargeFile.close();

    std::ifstream nowChargeFile(BATTERY_STATUS_NOW_PATH);
    if (!nowChargeFile.is_open()) {
        batteryString = "nobat";
        return;
    }
    long chargeNow = 0;
    nowChargeFile >> chargeNow;
    nowChargeFile.close();

    long chargePercent = chargeNow * 100 / chargeFull;

    batteryString = GREEN_ON_BLACK;
    if (chargePercent >= 0 && chargePercent < 10) {
        batteryString = RED_ON_BLACK;
        batteryString += BATTERY_0;
    } else if (chargePercent >= 10 && chargePercent < 20) {
        batteryString = YELLOW_ON_BLACK;
        batteryString += BATTERY_1;
    } else if (chargePercent >= 20 && chargePercent < 30) {
        batteryString = YELLOW_ON_BLACK;
        batteryString += BATTERY_2;
    } else if (chargePercent >= 30 && chargePercent < 40) {
        batteryString += BATTERY_3;
    } else if (chargePercent >= 40 && chargePercent < 50) {
        batteryString += BATTERY_4;
    } else if (chargePercent >= 50 && chargePercent < 60) {
        batteryString += BATTERY_5;
    } else if (chargePercent >= 60 && chargePercent < 70) {
        batteryString += BATTERY_6;
    } else if (chargePercent >= 70 && chargePercent < 80) {
        batteryString += BATTERY_7;
    } else if (chargePercent >= 80 && chargePercent < 90) {
        batteryString += BATTERY_8;
    } else if (chargePercent >= 90 && chargePercent <= 100) {
        batteryString += BATTERY_9;
    }
    batteryString += DEFAULT;
}
