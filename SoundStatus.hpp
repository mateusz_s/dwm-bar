#pragma once

#include "SystemStatus.hpp"

class SoundStatus : public SystemStatus {
public:
    const std::string &getString();
    void update();
    ~SoundStatus() {};
private:
    std::string soundString;
};
