#pragma once

#define CPU_ICON            "\ue223"

#define CLOCK_ICON          "\uf017"

#define SOUND_ICON          "\ue050"
#define SOUND_MUTE_ICON     "\ue04f"

#define BATTERY_0           "\ue242"
#define BATTERY_1           "\ue243"
#define BATTERY_2           "\ue244"
#define BATTERY_3           "\ue245"
#define BATTERY_4           "\ue246"
#define BATTERY_5           "\ue247"
#define BATTERY_6           "\ue248"
#define BATTERY_7           "\ue249"
#define BATTERY_8           "\ue24a"
#define BATTERY_9           "\ue24b"

