#pragma once

#include <string>

class SystemStatus {
public:
    virtual const std::string &getString() = 0; 
    virtual void update() = 0;
    virtual ~SystemStatus() {};
};
