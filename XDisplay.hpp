#pragma once

#include <X11/Xlib.h>
#include <memory>
#include <string>

class XDisplay {
public:
    static XDisplay &getInstance(); 
                    
    Display *getDisplay();
    void setStatus(std::string status);

private:
    XDisplay(const XDisplay &) = delete;
    XDisplay(const XDisplay &&) = delete;
    XDisplay(XDisplay &&) = delete;
    XDisplay(XDisplay &) = delete;
    XDisplay(); 
        
    std::unique_ptr<Display, std::function<void(Display *)>> display;
};
