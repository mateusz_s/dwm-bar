#include <chrono>
#include <iostream>
#include <memory>
#include <thread>
#include <vector>

#include <X11/Xlib.h>

#include "BatteryStatus.hpp"
#include "CpuStatus.hpp"
#include "SoundStatus.hpp"
#include "TimeStatus.hpp"
#include "XDisplay.hpp"

int main() {
    std::vector<std::unique_ptr<SystemStatus>> services;
    services.push_back(std::move(std::unique_ptr<CpuStatus>(new CpuStatus())));
    services.push_back(
        std::move(std::unique_ptr<TimeStatus>(new TimeStatus())));
    services.push_back(
        std::move(std::unique_ptr<SoundStatus>(new SoundStatus())));
    services.push_back(
        std::move(std::unique_ptr<BatteryStatus>(new BatteryStatus())));
    while (true) {
        std::string toDisplay = "";
        for (const auto &service : services) {
            service->update();
            toDisplay += service->getString();
        }

        XDisplay::getInstance().setStatus(toDisplay);
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}
