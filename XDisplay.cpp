#include "XDisplay.hpp"
#include <iostream>

XDisplay &XDisplay::getInstance() {
    static XDisplay instance;
    return instance;
}

Display *XDisplay::getDisplay() {
    return display.get();
}

XDisplay::XDisplay(): display(XOpenDisplay(NULL), [](Display *display) { if(nullptr != display) XCloseDisplay(display); }) {
    if (nullptr == display) {
        std::cerr << "Failed to open display" << std::endl;
    }
}

void XDisplay::setStatus(std::string status) {
    XStoreName(display.get(), DefaultRootWindow(display.get()), status.c_str());
    XSync(display.get(), false);
}

